.PHONY: build

build: 
	docker build -t registry.gitlab.com/jackhughes/friendly-tcp:latest -f build/Dockerfile .

build-server:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o friendly-server ./cmd/server

build-client:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o friendly-client ./cmd/client

run:
	docker run registry.gitlab.com/jackhughes/friendly-tcp:latest

login:
	docker login registry.gitlab.com

push: build
	docker push registry.gitlab.com/jackhughes/friendly-tcp:latest

lint: 
	golint -set_exit_status cmd/... 

vet:
	cd cmd && go vet ./...

fmt: 
	cd cmd && go fmt ./...

deadcode:
	cd cmd/server && deadcode .

misspell:
	cd cmd/ && misspell .

gocyclo:
	cd cmd/ && gocyclo -over 10 .

test:
	@mkdir -p artifacts
	cd internal/ && go test ./... -race -cover -coverprofile=../artifacts/coverage.out

coverage: test
	go tool cover -html=artifacts/coverage.out -o artifacts/coverage.html