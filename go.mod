module gitlab.com/jackhughes/friendly-tcp

go 1.14

require (
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.2.2
)
