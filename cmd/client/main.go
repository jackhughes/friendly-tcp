package main

import (
	"gitlab.com/jackhughes/friendly-tcp/cmd/client/cobra"
)

func main() {
	cobra.Execute()
}
