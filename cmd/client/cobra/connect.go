package cobra

import (
	"encoding/json"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/jackhughes/friendly-tcp/internal/client"
)

func init() {
	var (
		port    = "8080"
		address = "127.0.0.1"
		user    = 1
		friends = []int{1, 2, 3}
	)

	connect.Flags().StringVarP(&address, "address", "a", address, "the host address of the server that the client should connect to")
	connect.Flags().StringVarP(&port, "port", "p", port, "the port number that the client should connect to")
	connect.Flags().IntVarP(&user, "user", "u", user, "the connecting user id")
	connect.Flags().IntSliceVarP(&friends, "friends", "f", friends, "the list of friends of the user")

	viper.BindPFlag("address", connect.Flags().Lookup("address"))
	viper.BindPFlag("port", connect.Flags().Lookup("port"))
	viper.BindPFlag("user", connect.Flags().Lookup("user"))
	viper.BindPFlag("friends", connect.Flags().Lookup("friends"))

	rootCmd.AddCommand(connect)
}

var connect = &cobra.Command{
	Use:   "connect",
	Short: "runs the friendly tcp client",
	Long:  "",
	Run: func(cmd *cobra.Command, args []string) {
		logrus.Info("...initialising friendly-tcp client")

		host := viper.Get("address").(string)
		port := viper.Get("port").(string)
		usrID := viper.Get("user").(int)
		friends := viper.Get("friends").(string)

		var is []int
		if err := json.Unmarshal([]byte(friends), &is); err != nil {
			logrus.Fatalf("couldn't unmarshal string to []int, please format appropriately: %v", err)
		}

		c := client.NewClient(host, port, usrID, is)
		c.Connect()
	},
}
