package cobra

import (
	"errors"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/jackhughes/friendly-tcp/internal/server/tcp"
	"gitlab.com/jackhughes/friendly-tcp/internal/storage"
	"gitlab.com/jackhughes/friendly-tcp/internal/storage/inmem"
)

const (
	// InMem is the constant value for the in-memory storage type
	InMem = "inmem"
)

func init() {
	var (
		port    = "8080"
		address = "127.0.0.1"
		storage = "inmem"
	)

	run.Flags().StringVarP(&address, "address", "a", address, "the host address of the server that the client should run to")
	run.Flags().StringVarP(&port, "port", "p", port, "the port number that the client should run to")
	run.Flags().StringVarP(&storage, "storage", "s", storage, "the storage method to use")

	viper.BindPFlag("address", run.Flags().Lookup("address"))
	viper.BindPFlag("port", run.Flags().Lookup("port"))
	viper.BindPFlag("storage", run.Flags().Lookup("storage"))

	rootCmd.AddCommand(run)
}

var run = &cobra.Command{
	Use:   "run",
	Short: "runs the friendly tcp server",
	Long:  "",
	Run: func(cmd *cobra.Command, args []string) {
		logrus.Info("...initialising friendly-tcp server")

		host := viper.Get("address").(string)
		port := viper.Get("port").(string)
		storage := viper.Get("storage").(string)

		store, err := buildStorageEngine(storage)
		if err != nil {
			logrus.Fatalf("could not create storage engine: %v", err)
		}

		s := tcp.NewServer(host, port, store)
		s.Run()
	},
}

// Build an approproate storage engine, in this instance it is in memory
// storage.Storage is an interface, which means that this engine could easily be
// swapped out for something like an RDBMS solution, or flat storage solution like
// Elasticsearch or MongoDB
func buildStorageEngine(str string) (storage.Storage, error) {
	switch str {
	case InMem:
		db, err := inmem.NewStorage()
		if err != nil {
			return nil, err
		}

		return db, nil

	default:
		return nil, errors.New("unsupported storage engine")
	}
}
