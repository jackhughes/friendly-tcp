package main

import (
	"gitlab.com/jackhughes/friendly-tcp/cmd/server/cobra"
)

func main() {
	cobra.Execute()
}
