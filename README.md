[![pipeline status](https://gitlab.com/jackhughes/friendly-tcp/badges/master/pipeline.svg)](https://gitlab.com/jackhughes/friendly-tcp/commits/master)
[![coverage report](https://gitlab.com/jackhughes/friendly-tcp/badges/master/coverage.svg)](https://gitlab.com/jackhughes/friendly-tcp/commits/master)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/jackhughes/friendly-tcp/blob/master/LICENSE)

## Friendly TCP - Know the online status of your friends in real time

- [Friendly TCP - Know the online status of your friends in real time](#friendly-tcp---know-the-online-status-of-your-friends-in-real-time)
- [What does it do?](#what-does-it-do)
  - [The Server](#the-server)
  - [The Client](#the-client)
- [Installation Instructions](#installation-instructions)
  - [Requirements](#requirements)
    - [Docker](#docker)
  - [Testing](#testing)
- [Notable Features](#notable-features)
- [QoL Enhancements](#qol-enhancements)
- [Q/A](#qa)
  - [What changes if we switch TCP to UDP?](#what-changes-if-we-switch-tcp-to-udp)
  - [How would you detect the user (connection) is still online?](#how-would-you-detect-the-user-connection-is-still-online)
  - [What happens if the user has a lot of friends?](#what-happens-if-the-user-has-a-lot-of-friends)
  - [How design of your application will change if there will be more than one instance of the server](#how-design-of-your-application-will-change-if-there-will-be-more-than-one-instance-of-the-server)

## What does it do?
Friendly-tcp is a client/server implementation. The two command line applications do different things.

### The Server
-   Accepts incoming connections over TCP
-   Stores client data in an in-memory data store
    -   If the user is sent twice, the application will overwrite it's existing friends list
-   The server will send messages back to connected clients when the online status of one of their friends changes (i.e. online/offline)

### The Client
-   A CLI tool that allows configurable connectivity to an upstream server
-   The client sends a user id and array of friend ids to the server for storage
-   The client receives notification from the server when one of those friends connects/disconnects from the server
  
## Installation Instructions
### Requirements
-   Go 1.14
-   Go Modules
-   Docker (not a strict requirement)

To build the client and server - simply run `make build-client` and `make build-server` respectively.

Once these binaries exist, you should run the server first with the following command:

`./friendly-server run --address 127.0.0.1 --port 8080 --storage inmem`

You can then connect a client by using the following command:

`./friendly-client connect --address 127.0.0.1 --port 8080 --user 1 --friends 1,2,3`

Monitor the logs as the application ticks away!

#### Docker
You can also run the server in a Docker container, this implementation exists so that the application can be deployed to a Kubernetes cluster (or other container orchestration platform). However, the current in-memory storage solution means that this is currently not viable.

### Testing
Simply run `make coverage` to run the unit tests and generate a full coverage report.

## Notable Features
-   CI/CD Pipelines
-   Docker Container
-   Concurrent TCP Server

## QoL Enhancements
-   Integration test the client and server by running the two in a virtua environment and testing the input and output from the two
-   Swap in-memory storage for a more robust solution, either a RBDMS such as PostgreSQL or flat document storage like Elasticsearch or MongoDB so the application can be deployed to a solution like Kubernetes and scale horizontally
## Q/A
### What changes if we switch TCP to UDP?
-   TCP is a connection orientated protocol, if we switch to UDP, that is the end of the relationship between the client and server
    -   If we switched the implementation here to UDP, we would need to create a server on the client that can also accept messages from the server implementation. As there is no "connection" we can only direct response packets back to the client address registered with the server when packets were initially sent
-   TCP is suited for reliability, where the round trip time of the packets is not a large factor, TCP is by nature slower than UDP
-   UDP is more suited to fast, efficient transmission. Namely online multiplayer games or low latency applications running on edge nodes
-   TCP sends packets in order, UDP does not enforce this
-   UDP means there is no guarentee that packets will be received by the server
-   UDP discards erroneous packets whereas TCP does error checking and recovery
-   TCP initiates a handshake between the client and server, UDP does not
-   
### How would you detect the user (connection) is still online?
-   In this implementation, we hold the active connection in memory and remove it when the user disconnects
-   We could additionally set a keep-alive property on the connection to ensure it remains active indefinitely or for a time period
-   Alongside this, we could ping the connection to ensure that packets can be sent between the client and server before transmission. This would increase network overhead
-   Additionaly, we could set a read deadline on the connection to establish a timeout for receiving messages on the connection
### What happens if the user has a lot of friends?
-   In this implementation, the program becomes slower and slower the more friends that a user has, as it has to iterate over all users and all of their friends
-   The application will continue to use more memory as the list of users and friends grows
-   A solution would be to outsource this processing to a remote storage backend. We should use something like Elasticsearch where speed is a requirement due to the nature of information being flat, indexed, and stored in memory. We should set primary and secondary indexes on the UserID and FriendIDs in order to make the search more efficient
  
### How design of your application will change if there will be more than one instance of the server
-   This has already been considered in the implementation of this application for the most part
    -   The storage implementation uses an interface so that we can easily swap out the in memory storage for an RDBMS (SQL) or flat document storage (Elasticsearch, MongoDB, etc)
-   As above I would move the storage implementation to a centralised store that all instances of the server could communicate with
-   I would deploy the server to Kubernetes and add a Load Balancer in front of those running deployments. Clients would then connect to the public IP address of that Load Balancer and connections would be distribted amongst several running deployments