package storage

import (
	"net"
	"sync"

	"gitlab.com/jackhughes/friendly-tcp/internal/server"
)

// InMemoryStorage contains a slice of all users and a mutex
// to maintain concurrency safety
type InMemoryStorage struct {
	Users []User
	Mutex sync.Mutex
}

// User struct contains the user id, associated friend ids and
// the TCP connection
type User struct {
	UserID  int
	Friends []int
	TCPConn net.Conn
}

// Storage is the default generic interface for the server
type Storage interface {
	// Create a new user, pass the type and return an error on failure
	Create(user *server.UserFriends) ([]User, error)
	// GetOnlineFriends gets all users where there is still an active TCP connection
	GetOnlineFriends(usrID int) []server.UserFriends
	// GetUserIDFromConnection gets all users where there is still an active TCP connection
	GetUserIDFromConnection(conn net.Conn) (int, error)
}
