package inmem

import (
	"errors"
	"net"

	"github.com/sirupsen/logrus"
	"gitlab.com/jackhughes/friendly-tcp/internal/server"
	"gitlab.com/jackhughes/friendly-tcp/internal/storage"
)

// Storage is an implementation of the generic interface
// storage.Storage for an in-memory storage solution
type Storage struct {
	db *storage.InMemoryStorage
}

// NewStorage returns an instance of a storage engine
func NewStorage() (storage.Storage, error) {
	return &Storage{db: &storage.InMemoryStorage{}}, nil
}

// Create an in memory user
func (s *Storage) Create(usr *server.UserFriends) ([]storage.User, error) {
	logrus.Infof("attempting to store user id: %v", usr.UserID)

	u := storage.User{
		UserID:  usr.UserID,
		Friends: usr.FriendIds,
		TCPConn: usr.TCPConn,
	}

	exists, key := FindUser(s.db.Users, u.UserID)
	if key > -1 {
		logrus.Infof("user: %v already exists, updating friends", exists.UserID)

		s.db.Mutex.Lock()
		s.db.Users[key] = u
		s.db.Mutex.Unlock()

		return s.db.Users, nil
	}

	s.db.Mutex.Lock()
	s.db.Users = append(s.db.Users, u)
	s.db.Mutex.Unlock()

	logrus.Infof("succesfully added user to storage: %v", usr.UserID)

	return s.db.Users, nil
}

// GetOnlineFriends gets online users that have the user id as a friend
func (s *Storage) GetOnlineFriends(usrID int) []server.UserFriends {
	logrus.Infof("attempting to fetch online users that have user id: %v as a friend", usrID)

	return FindFriends(s.db.Users, usrID)
}

// GetUserIDFromConnection gets the associated user id from a TCP connection
func (s *Storage) GetUserIDFromConnection(conn net.Conn) (int, error) {
	logrus.Info("closed connection detected, getting user that dropped")

	return FindUserIDFromConnection(s.db.Users, conn)
}

// FindUser gets a user from the retrieved in-memory data store and returns the struct
func FindUser(slice []storage.User, usrID int) (storage.User, int) {
	for i := range slice {
		if slice[i].UserID == usrID {
			return slice[i], i
		}
	}

	return storage.User{}, -1
}

// FindFriends gets users with an active TCP connection from the in-memory database
func FindFriends(usrs []storage.User, usrID int) []server.UserFriends {
	users := []server.UserFriends{}
	for _, usr := range usrs {
		if usr.TCPConn != nil {
			for _, v := range usr.Friends {
				if v == usrID {
					users = append(users, server.UserFriends{
						UserID:    usr.UserID,
						FriendIds: usr.Friends,
						TCPConn:   usr.TCPConn,
					})
				}
			}
		}
	}

	return users
}

// FindUserIDFromConnection gets an associated user id to a TCP connection
func FindUserIDFromConnection(usrs []storage.User, conn net.Conn) (int, error) {
	for _, usr := range usrs {
		if usr.TCPConn == conn {
			return usr.UserID, nil
		}
	}

	return 0, errors.New("couldn't find user associated with tcp connection")
}
