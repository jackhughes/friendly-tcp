package inmem

import (
	"errors"
	"net"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/jackhughes/friendly-tcp/internal/server"
	"gitlab.com/jackhughes/friendly-tcp/internal/storage"
)

func init() {
	go func() {
		srv, err := net.Listen("tcp", ":1100")
		if err != nil {
			return
		}
		for {
			conn, err := srv.Accept()
			if err != nil {
				err = errors.New("could not accept connection")
				break
			}
			if conn == nil {
				err = errors.New("could not create connection")
				break
			}
		}
	}()
}

func newServerConnection() net.Conn {
	conn, err := net.Dial("tcp", ":1100")
	if err != nil {
		logrus.Fatalf("couldn't connect to upstream tcp server: %err", err)
	}

	return conn
}

func TestCreateNewUser(t *testing.T) {
	s, err := NewStorage()
	assert.NoError(t, err)

	tests := []struct {
		userID  int
		friends []int
		result  []storage.User
	}{
		{
			userID:  1,
			friends: []int{1, 2, 3},
			result: []storage.User{
				storage.User{
					UserID:  1,
					Friends: []int{1, 2, 3},
					TCPConn: nil,
				},
			},
		},
	}
	for _, tt := range tests {
		usr := &server.UserFriends{
			UserID:    tt.userID,
			FriendIds: tt.friends,
		}
		u, err := s.Create(usr)
		assert.NoError(t, err)
		assert.Equal(t, tt.result, u)
	}
}

func TestCreateExistingUser(t *testing.T) {
	s, err := NewStorage()
	assert.NoError(t, err)

	tests := []struct {
		userID  int
		friends []int
		result  []storage.User
	}{
		{
			userID:  1,
			friends: []int{1, 2, 3},
			result: []storage.User{
				storage.User{
					UserID:  1,
					Friends: []int{1, 2, 3},
					TCPConn: nil,
				},
			},
		},
		{
			userID:  1,
			friends: []int{3, 2, 1},
			result: []storage.User{
				storage.User{
					UserID:  1,
					Friends: []int{3, 2, 1},
					TCPConn: nil,
				},
			},
		},
	}
	for _, tt := range tests {
		usr := &server.UserFriends{
			UserID:    tt.userID,
			FriendIds: tt.friends,
		}
		u, err := s.Create(usr)
		assert.NoError(t, err)
		assert.Equal(t, tt.result, u)
	}
}

func TestFindFriends(t *testing.T) {
	con := newServerConnection()
	inmemDB := []storage.User{
		storage.User{
			UserID:  1,
			Friends: []int{1, 2, 3},
			TCPConn: con,
		},
	}
	tests := []struct {
		userID  int
		friends []int
		result  []server.UserFriends
	}{
		{
			userID: 1,
			result: []server.UserFriends{
				server.UserFriends{
					UserID:    1,
					FriendIds: []int{1, 2, 3},
					TCPConn:   con,
				},
			},
		},
	}
	for _, tt := range tests {
		uf := FindFriends(inmemDB, tt.userID)
		assert.Equal(t, tt.result, uf)
	}
}

func TestFindUserIDFromConnection(t *testing.T) {
	con := newServerConnection()
	tests := []struct {
		connec net.Conn
		usrs   []storage.User
		result int
	}{
		{
			connec: con,
			usrs: []storage.User{
				storage.User{
					UserID:  1,
					Friends: []int{1, 2, 3},
					TCPConn: con,
				},
			},
			result: 1,
		},
	}
	for _, tt := range tests {
		uid, err := FindUserIDFromConnection(tt.usrs, tt.connec)
		assert.NoError(t, err)
		assert.Equal(t, tt.result, uid)
	}
}
