package client

import (
	"bufio"
	"encoding/json"
	"fmt"
	"net"

	"github.com/sirupsen/logrus"
	"gitlab.com/jackhughes/friendly-tcp/internal/server"
)

// Client struct for holding client config
type Client struct {
	DialAddr string
	User     server.UserFriends
}

// NewClient instantiates a new client
func NewClient(dialAddr string, dialPort string, usrID int, friends []int) *Client {
	return &Client{
		DialAddr: net.JoinHostPort(dialAddr, dialPort),
		User: server.UserFriends{
			UserID:    usrID,
			FriendIds: friends,
		},
	}
}

// Connect connects to a remote TCP server and sends a payload,
// whilst waiting for a response from the server, this functions is blocking
func (c *Client) Connect() {
	conn, err := net.Dial(server.ServerProtocol, c.DialAddr)
	if err != nil {
		logrus.Fatalf("couldn't connect to upstream tcp server: %err", err)
	}

	payload, err := json.Marshal(c.User)
	if err != nil {
		logrus.Fatalf("couldn't marshal data structure: %v", err)
	}

	pl := string(payload)
	logrus.Infof("writing payload to server: %v", pl)
	fmt.Fprintf(conn, pl+"\n")

	for {
		message, _ := bufio.NewReader(conn).ReadString('\n')

		if message != "" {
			logrus.Infof("%v", message)
		}
	}
}
