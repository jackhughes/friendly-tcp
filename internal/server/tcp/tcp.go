package tcp

import (
	"bufio"
	"encoding/json"
	"fmt"
	"net"

	"github.com/sirupsen/logrus"
	"gitlab.com/jackhughes/friendly-tcp/internal/server"
	"gitlab.com/jackhughes/friendly-tcp/internal/storage"
)

const (
	// StatusOnline when a client connects
	StatusOnline = "online"
	// StatusOffline when a client disconnects
	StatusOffline = "offline"
)

// Server contains an address to run on and a storage engine
type Server struct {
	Addr string
	Db   storage.Storage
}

// NewServer instantiates a new server struct
func NewServer(addr string, port string, storage storage.Storage) *Server {
	return &Server{
		Addr: net.JoinHostPort(addr, port),
		Db:   storage,
	}
}

// Run the new server instance and listen for TCP connections
func (s *Server) Run() {
	lstr, err := net.Listen(server.ServerProtocol, s.Addr)
	if err != nil {
		logrus.Fatalf("couldn't start tcp listener: %v", err)
	}

	for {
		conn, err := lstr.Accept()
		if err != nil {
			logrus.Errorf("failed to accept tcp connection: %v", err)
		}

		go handle(conn, s.Db)
	}
}

// Concurrently handle incoming connections
func handle(conn net.Conn, db storage.Storage) {
	bytes, err := bufio.NewReader(conn).ReadBytes('\n')
	if err != nil {
		// The connection has been closed by the client or timed out
		// Find out which one it was and alert their online friends
		usrID, err := db.GetUserIDFromConnection(conn)
		if err != nil {
			logrus.Errorf("failed to get user id: %v", err)

			return
		}

		conn.Close()
		logrus.Infof("user %v disconnected, notifying online friends", usrID)
		notifyFriends(db, usrID, StatusOffline)

		// escape recursion
		return
	}

	u := &server.UserFriends{}
	err = json.Unmarshal(bytes, u)
	if err != nil {
		logrus.Fatalf("unmarshaller error: %v", err)
		conn.Close()

		// escape recursion
		return
	}

	u.TCPConn = conn
	_, err = db.Create(u)
	if err != nil {
		logrus.Errorf("failed to store user: %v", err)
	}

	notifyFriends(db, u.UserID, StatusOnline)

	// recursive func to handle random disconnects
	handle(conn, db)
}

// Send a message to notify friends of online/offline events
func notifyFriends(db storage.Storage, usrID int, status string) {
	friends := db.GetOnlineFriends(usrID)
	for _, v := range friends {
		response := fmt.Sprintf("user %v %v \n", usrID, status)
		v.TCPConn.Write([]byte(response))
	}
}
