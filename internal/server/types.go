package server

import "net"

// ServerProtocol is the protocol to run the server on
const ServerProtocol = "tcp"

// UserFriends is a struct that allows payloads to be unmarshalled
type UserFriends struct {
	// UserID is the unique user id
	UserID int `json:"user_id"`
	// FriendIDs are the unique ids of associated friends
	FriendIds []int `json:"friends"`
	// TCPConn holds the initial TCP connection
	TCPConn net.Conn `json:"tcpconn,omitempty"`
}
